#Constellation Series#

## Summary ##

**Constellation Series** is a bridge package between the [TMDB](https://www.themoviedb.org/?language=fr) site and Constellation. Thanks to the web interface you will be able to see the trending series or search series and subscribe to them. 

This package will check with a given interval if a new episode is airing today.

## Credits ##

- Nicolas Creton
- François Decan
- Baptiste Karolewski
- Marine Ripon

**ISEN Lille - 2016**