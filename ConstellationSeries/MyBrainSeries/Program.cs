﻿using Constellation;
using Constellation.Control;
using Constellation.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyBrainSeries
{
    public class Program : PackageBase
    {
        List<int> viewedEpisodes;

        [StateObjectLink(Package = "ConstellationSeries")]
        public StateObjectNotifier stateEpisodes { get; set; }

        [StateObjectLink("MyBrainSeries", "*")]
        public StateObjectNotifier myBrainepisodesState { get; set; }

        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        public override void OnStart()
        {
            PackageHost.WriteInfo("Package starting - IsRunning: {0} - IsConnected: {1}", PackageHost.IsRunning, PackageHost.IsConnected);
            if (PackageHost.HasControlManager)
            {
                PackageHost.ControlManager.RegisterStateObjectLinks(this);
                PackageHost.WriteInfo("ControlHub access granted");
            }
            this.viewedEpisodes = new List<int>();
            this.myBrainepisodesState.ValueChanged += myBrainepisodesState_ValueChanged;
            this.stateEpisodes.ValueChanged += stateEpisodes_ValueChanged;
        }

        void myBrainepisodesState_ValueChanged(object sender, StateObjectChangedEventArgs e)
        {
            this.viewedEpisodes.Add((int)e.NewState.DynamicValue.id);
        }

        void stateEpisodes_ValueChanged(object sender, StateObjectChangedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                //On attends 2 secondes le temps que la liste des épisodes déjà notifiés se charge au démarrage de l'appli
                Thread.Sleep(2000);
                string isEpisode = "isEpisode";
                string yes = "yes";
                //Tri des SO afin de ne recevoir que les SO relatifs aux épisodes du jour
                if (e.NewState.Metadatas.TryGetValue(isEpisode, out yes) == true)
                {
                    //On vérifie si le user n'a pas déjà été notifié
                    if (!this.viewedEpisodes.Contains((int)e.NewState.DynamicValue.idEpisode))
                    {
                        //On envoie a PushBullet si un nouvel épisode sort ojd
                        PackageHost.CreateScope("PushBullet").Proxy.SendPush(
                            new
                            {
                                Title = e.NewState.DynamicValue.titleSerie + " Saison " + e.NewState.DynamicValue.seasonNumber + " Episode " + e.NewState.DynamicValue.episodeNumber,
                                Message = e.NewState.DynamicValue.summary
                            });
                        //Push d'un SO afin de se souvenir si le user a déjà été notifié pour cet épidose
                        PackageHost.PushStateObject(e.NewState.Name + "MyBrainEpisode", new { id = e.NewState.DynamicValue.idEpisode }); 
                    }
                    else
                    {
                        PackageHost.WriteInfo("La notif a déjà été envoyée");
                    }
                }
            });
        }
    }
}
