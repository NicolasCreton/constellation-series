﻿/*
 *	 Constellation Platform 1.7
 *	 Web site: http://www.myConstellation.io
 *	 Copyright (C) 2014-2015 - Sebastien Warin <http://sebastien.warin.fr>	   	
 *	 All Rights Reserved.  
 *	
 *	 NOTICE:  All information contained herein is, and remains the property of Sebastien Warin.
 *	 The intellectual and technical concepts contained herein are proprietary to Sébastien Warin.
 *	 Dissemination of this information or reproduction of this material is strictly forbidden
 *   unless prior written permission is obtained from Sébastien Warin.	 
*/

/// <reference path="jquery-2.1.3.js" />
/// <reference path="jquery.signalR-2.2.0.js" />
(function ($, window, undefined) {
    /// <param name="$" type="jQuery" />
    "use strict";

    if (typeof ($.signalR) !== "function") {
        throw new Error("SignalR: SignalR is not loaded. Please ensure jquery.signalR-x.js is referenced before ~/signalr/js.");
    }

    var signalR = $.signalR;
    var groups = {};

    function makeProxyCallback(hub, callback) {
        return function () {
            // Call the client hub method
            callback.apply(hub, $.makeArray(arguments));
        };
    }

    function registerHubProxies(instance, shouldSubscribe) {
        var key, hub, memberKey, memberValue, subscriptionMethod;

        for (key in instance) {
            if (instance.hasOwnProperty(key)) {
                hub = instance[key];

                if (!(hub.hubName)) {
                    // Not a client hub
                    continue;
                }

                if (shouldSubscribe) {
                    // We want to subscribe to the hub events
                    subscriptionMethod = hub.on;
                } else {
                    // We want to unsubscribe from the hub events
                    subscriptionMethod = hub.off;
                }

                // Loop through all members on the hub and find client hub functions to subscribe/unsubscribe
                for (memberKey in hub.client) {
                    if (hub.client.hasOwnProperty(memberKey)) {
                        memberValue = hub.client[memberKey];

                        if (!$.isFunction(memberValue)) {
                            // Not a client hub function
                            continue;
                        }

                        subscriptionMethod.call(hub, memberKey, makeProxyCallback(hub, memberValue));
                    }
                }
            }
        }
    }

    $.hubConnection.prototype.createHubProxies = function () {
        var proxies = {};
        this.starting(function () {
            // Register the hub proxies as subscribed. (instance, shouldSubscribe)
            registerHubProxies(proxies, true);

            this._registerSubscribedHubs();
        }).disconnected(function () {
            // Unsubscribe all hub proxies when we "disconnect".  This is to ensure that we do not re-add functional call backs. (instance, shouldSubscribe)
            registerHubProxies(proxies, false);
        });
        proxies['Constellation'] = this.createHubProxy('controlHub');
        proxies['Constellation'].client = {
            onReceiveLogMessage: function (callback) {
                groups.PackagesLog = true;
                return proxies['Constellation'].on("SendWriteLog", callback);
            },
            onUpdateSentinel: function (callback) {
                groups.Sentinels = true;
                return proxies['Constellation'].on("UpdateSentinel", callback);
            },
            onUpdateSentinelsList: function (callback) {
                return proxies['Constellation'].on("UpdateSentinelsList", callback);
            },
            onReportPackageState: function (callback) {
                groups.PackagesState = true;
                return proxies['Constellation'].on("ReportPackageState", callback);
            },
            onReportPackageUsage: function (callback) {
                groups.PackagesUsage = true;
                return proxies['Constellation'].on("ReportPackageUsage", callback);
            },
            onUpdatePackageList: function (callback) {
                return proxies['Constellation'].on("UpdatePackageList", callback);
            },
            onReceiveMessage: function (callback) {
                return proxies['Constellation'].on("SendMessage", callback);
            },
            onUpdateStateObject: function (callback) {
                return proxies['Constellation'].on("UpdateStateObject", callback);
            },
            onUpdatePackageDescriptor: function (callback) {
                return proxies['Constellation'].on("UpdatePackageDescriptor", callback); 
            }
        };
        proxies['Constellation'].server = {
            addToControlGroup: function (group) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["AddToControlGroup"], $.makeArray(arguments)));
            },

            purgeStateObjects: function (sentinelName, packageName, name, type) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["PurgeStateObjects"], $.makeArray(arguments)));
            },

            refreshServerConfiguration: function (applyConfiguration) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["RefreshServerConfiguration"], $.makeArray(arguments)));
            },

            reload: function (sentinelName, packageName) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["Reload"], $.makeArray(arguments)));
            },

            removeToControlGroup: function (group) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["RemoveToControlGroup"], $.makeArray(arguments)));
            },

            requestPackageDescriptor: function (packageName) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["RequestPackageDescriptor"], $.makeArray(arguments)));
            },

            requestPackagesList: function (sentinelName) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["RequestPackagesList"], $.makeArray(arguments)));
            },

            requestSentinelsList: function () {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["RequestSentinelsList"], $.makeArray(arguments)));
            },

            requestSentinelUpdates: function () {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["RequestSentinelUpdates"], $.makeArray(arguments)));
            },

            requestStateObjects: function (sentinelName, packageName, name, type) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["RequestStateObjects"], $.makeArray(arguments)));
            },

            restart: function (sentinelName, packageName) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["Restart"], $.makeArray(arguments)));
            },

            sendMessage: function (scope, key, data) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["SendMessage"], $.makeArray(arguments)));
            },

            start: function (sentinelName, packageName) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["Start"], $.makeArray(arguments)));
            },

            stop: function (sentinelName, packageName) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["Stop"], $.makeArray(arguments)));
            },

            subscribeMessages: function (group) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["SubscribeMessages"], $.makeArray(arguments)));
            },

            subscribeStateObjects: function (sentinelName, packageName, name, type) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["SubscribeStateObjects"], $.makeArray(arguments)));
            },

            unSubscribeMessages: function (group) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["UnSubscribeMessages"], $.makeArray(arguments)));
            },

            unSubscribeStateObjects: function (sentinelName, packageName, name, type) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["UnSubscribeStateObjects"], $.makeArray(arguments)));
            },

            updatePackageSettings: function (sentinelName, packageName) {
                return proxies['Constellation'].invoke.apply(proxies['Constellation'], $.merge(["UpdatePackageSettings"], $.makeArray(arguments)));
            }
        };

        return proxies;
    };
    
    signalR.createConstellationClient = function (serverUri, accessKey, friendlyName) {
        signalR.hub = $.hubConnection(serverUri, {
            useDefaultPath: true,
            qs: {
                "SentinelName": "ControlHub",
                "PackageName": friendlyName,
                "AccessKey": accessKey
            }
        });
        signalR.hub.stateChanged(function (change) {
            if (change.newState === $.signalR.connectionState.connected) {
                for (var group in groups) {
                    if (groups[group] == true) {
                        signalR.Constellation.server.addToControlGroup(group);
                    }
                }
            }
        });
        $.extend(signalR, signalR.hub.createHubProxies());
        return signalR.Constellation;
    };

}(window.jQuery, window));