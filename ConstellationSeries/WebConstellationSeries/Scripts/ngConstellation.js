﻿/*
 *	 Constellation Platform 1.7
 *	 Web site: http://www.myConstellation.io
 *	 Copyright (C) 2014-2015 - Sebastien Warin <http://sebastien.warin.fr>	   	
 *	 All Rights Reserved.  
 *	
 *	 NOTICE:  All information contained herein is, and remains the property of Sebastien Warin.
 *	 The intellectual and technical concepts contained herein are proprietary to Sébastien Warin.
 *	 Dissemination of this information or reproduction of this material is strictly forbidden
 *   unless prior written permission is obtained from Sébastien Warin.	 
*/

angular.module('ngConstellation', ['ng'])
    .constant('CONSTELLATION_VERSION', '1.7.0')
    .constant('MODULE_VERSION', '1.0.0')
	.value('$', $)
    .factory('constellation', ['$', '$rootScope', function ($, $rootScope) {
		        var constellationProxy = {
					constellationClient: null,
					onConnectionStateChangedCallback: null,
					intializeClient: function (serverUri, accessKey, friendlyName) {					
						constellationProxy.constellationClient = $.signalR.createConstellationClient(serverUri, accessKey, friendlyName);
						constellationProxy.constellationClient.connection.stateChanged(function (change) {							
							if(onConnectionStateChangedCallback != null) {
								onConnectionStateChangedCallback(change);
							}
						});
					},					
					connect: function () {
		                constellationProxy.constellationClient.connection.start();
		            },
					onConnectionStateChanged: function (callback) {
						onConnectionStateChangedCallback = callback;
					},
					onReceiveLogMessage: function (callback) {
						return constellationProxy.constellationClient.client.onReceiveLogMessage(callback);
					},
					onUpdateSentinel: function (callback) {
						return constellationProxy.constellationClient.client.onUpdateSentinel(callback);
					},
					onUpdateSentinelsList: function (callback) {
						return constellationProxy.constellationClient.client.onUpdateSentinelsList(callback);
					},
					onReportPackageState: function (callback) {
						return constellationProxy.constellationClient.client.onReportPackageState(callback);
					},
					onReportPackageUsage: function (callback) {
						return constellationProxy.constellationClient.client.onReportPackageUsage(callback);
					},
					onUpdatePackageList: function (callback) {
						return constellationProxy.constellationClient.client.onUpdatePackageList(callback);
					},
					onReceiveMessage: function (callback) {
						return constellationProxy.constellationClient.client.onReceiveMessage(callback);
					},
					onUpdateStateObject: function (callback) {
						return constellationProxy.constellationClient.client.onUpdateStateObject(callback);
					},
					onUpdatePackageDescriptor: function (callback) {
					    return constellationProxy.constellationClient.client.onUpdatePackageDescriptor(callback);
					},
					addToControlGroup: function (group) {
						return constellationProxy.constellationClient.server.addToControlGroup(group);
					},
					purgeStateObjects: function (sentinelName, packageName, name, type) {
						return constellationProxy.constellationClient.server.purgeStateObjects(sentinelName, packageName, name, type);
					},
					refreshServerConfiguration: function (applyConfiguration) {
						return constellationProxy.constellationClient.server.refreshServerConfiguration(applyConfiguration);
					},
					reload: function (sentinelName, packageName) {
						return constellationProxy.constellationClient.server.reload(sentinelName, packageName);
					},
					removeToControlGroup: function (group) {
						return constellationProxy.constellationClient.server.removeToControlGroup(group);
					},
					requestPackageDescriptor: function (packageName) {
					    return constellationProxy.constellationClient.server.requestPackageDescriptor(packageName);
					},
					requestPackagesList: function (sentinelName) {
						return constellationProxy.constellationClient.server.requestPackagesList(sentinelName);
					},
					requestSentinelUpdates: function () {
						return constellationProxy.constellationClient.server.requestSentinelUpdates();
					},
					requestSentinelsList: function () {
						return constellationProxy.constellationClient.server.requestSentinelsList();
					},
					requestStateObjects: function (sentinelName, packageName, name, type) {
						return constellationProxy.constellationClient.server.requestStateObjects(sentinelName, packageName, name, type);
					},
					restart: function (sentinelName, packageName) {
						return constellationProxy.constellationClient.server.restart(sentinelName, packageName);
					},
					sendMessage: function (scope, key, data) {
						return constellationProxy.constellationClient.server.sendMessage(scope, key, data);
					},
					start: function (sentinelName, packageName) {
						return constellationProxy.constellationClient.server.start(sentinelName, packageName);
					},
					stop: function (sentinelName, packageName) {
						return constellationProxy.constellationClient.server.stop(sentinelName, packageName);
					},
					subscribeMessages: function (group) {
						return constellationProxy.constellationClient.server.subscribeMessages(group);
					},
					subscribeStateObjects: function (sentinelName, packageName, name, type) {
						return constellationProxy.constellationClient.server.subscribeStateObjects(sentinelName, packageName, name, type);
					},
					unSubscribeMessages: function (group) {
						return constellationProxy.constellationClient.server.unSubscribeMessages(group);
					},
					unSubscribeStateObjects: function (sentinelName, packageName, name, type) {
						return constellationProxy.constellationClient.server.unSubscribeStateObjects(sentinelName, packageName, name, type);
					},
					updatePackageSettings: function (sentinelName, packageName) {
						return constellationProxy.constellationClient.server.updatePackageSettings(sentinelName, packageName);
					},					
					requestSubscribeStateObjects: function (sentinelName, packageName, name, type) {
						constellationProxy.constellationClient.server.requestStateObjects(sentinelName, packageName, name, type);
						return constellationProxy.constellationClient.server.subscribeStateObjects(sentinelName, packageName, name, type);
					},
		        };
		        return constellationProxy;
		    }]);