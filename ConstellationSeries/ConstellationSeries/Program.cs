﻿using Constellation.Control;
using Constellation.Host;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TMDbLib.Client;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Search;
using TMDbLib.Objects.TvShows;

namespace ConstellationSeries
{
    public class Program : PackageBase
    {
        [StateObjectLink(Package = "ConstellationSeries")]
        public StateObjectCollectionNotifier seriesStateObjects { get; set; }
        List<Tuple<int, string>> ListFollowed;
        int counter = 0;

        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        public override void OnStart()
        {
            PackageHost.WriteInfo("Package starting - IsRunning: {0} - IsConnected: {1}", PackageHost.IsRunning, PackageHost.IsConnected);
            {
                if (PackageHost.HasControlManager)
                {
                    // Change the package manifest (PackageInfo.xml) to enable the control hub access
                    // Register the StateObjectLinks on this class
                    PackageHost.ControlManager.RegisterStateObjectLinks(this);
                    PackageHost.WriteInfo("ControlHub access granted");
                }
                //On initialise la liste de id que le user follow
                ListFollowed = new List<Tuple<int, string>>();

                //Récupération des SO au démarrage du package et lors d'update
                this.seriesStateObjects.ValueChanged += seriesStateObjects_ValueChanged;

                string checkInterval = PackageHost.GetSettingValue<String>("checkInterval");
                int delay;
                if (int.TryParse(checkInterval, out delay) == false ||  delay < 30)
                {
                    delay = 30;
                }
                Task.Factory.StartNew(() =>
                {
                    while (PackageHost.IsRunning)
                    {
                        //Toutes les 30 secondes mini
                        checkNewEpisode(delay);
                        Thread.Sleep(1000);
                    }
                });
            }
        }

        void seriesStateObjects_ValueChanged(object sender, StateObjectChangedEventArgs e)
        {
            //Récupération des StateObjects au démarrage et à chaque modif d'un SO, puis insertion dans la liste idListFollowed
            //Tri des SO afin de ne pas avoir ceux qui correspondent à des épisodes
            string isEpisode = "isEpisode";
            string yes = "yes";

            if (e.NewState.Metadatas.TryGetValue(isEpisode,out yes) == false)
            {
                this.ListFollowed.Add(new Tuple<int, string>((int)e.NewState.DynamicValue.Id, (string)e.NewState.DynamicValue.Name));
            }
        }

        public TMDbClient connectToAPI()
        {
            string apiKey = PackageHost.GetSettingValue<string>("api_key");
            TMDbClient client = new TMDbClient(apiKey);
            return client;
        }

        [MessageCallback]
        public void searchSeries(String serie)
        {
            if (MessageContext.Current.IsSaga)
            {
                TMDbClient client = connectToAPI();
                SearchContainer<SearchTv> seriesListSearch = client.SearchTvShow(serie);
                MessageContext.Current.SendResponse(new { Result = seriesListSearch });
            }
        }

        [MessageCallback]
        public void pushSerieToFollow(int idSerie)
        {
            TMDbClient client = connectToAPI();
            TvShow tvById = client.GetTvShow(idSerie);
            PackageHost.WriteInfo("Série récupérée : {0} : {1}", idSerie, tvById.Name);
            PackageHost.PushStateObject(tvById.Name, new { Id = tvById.Id, Name = tvById.Name, PosterPath = tvById.PosterPath });
        }

        [MessageCallback]
        public void getSerieDetails(int idSerie)
        {
            if (MessageContext.Current.IsSaga)
            {
                TMDbClient client = connectToAPI();

                //Détails sur la série
                TvShow tvDetails = client.GetTvShow(idSerie);
                //Envoi d'un objet contenant les infos générales sur la série
                MessageContext.Current.SendResponse(new { SerieDetail = tvDetails });

                //Détails sur les saisons
                int nbSeasons = tvDetails.NumberOfSeasons;

                for (int season = 1; season <= nbSeasons; season++)
                {
                    TvSeason seasonDetail = client.GetTvSeason(idSerie, season);
                    MessageContext.Current.SendResponse(new { Season = seasonDetail });
                }
            }
        }

        //Récupère la date du prochain épisode qui sera diffusé et donc à notifier !
        public TvEpisode getDateLastEpisodeToWatch(int idSerie)
        {
            TMDbClient client = connectToAPI();
            TvShow tvDetails = client.GetTvShow(idSerie);
            int nbSeasons = tvDetails.NumberOfSeasons;
            TvSeason lastCurrentSeason;
            TvSeason lastSeason = client.GetTvSeason(idSerie, nbSeasons); //Dernière saison de l'API
            //Mais, est-ce une saison en avance (connue ou en tournage) ou la saison en cours ?
            int reduceEpisodeIndex = 2;
            int reduceSeasonIndex = 1;

            int nbEpisodesLastSeason = lastSeason.EpisodeCount;
            TvEpisode lastWatchedEpisode = lastSeason.Episodes[nbEpisodesLastSeason - 1];
            TvEpisode lastEpisode = lastWatchedEpisode; //Juste pour l'initialisation (valeurs possibles : date du dernier épisode < date en cours(ne pas notifier) ou mêmes dates !)

            //Si date du dernier épisode récupéré de l'API est plus récent (futur) que la date en cours
            if (DateTime.Compare((DateTime)lastWatchedEpisode.AirDate.Date, DateTime.Now.Date) > 0)
            {
                while (DateTime.Compare((DateTime)lastWatchedEpisode.AirDate.Date, DateTime.Now.Date) > 0)
                {
                    //Si on a une ou plusieurs saisons en avance, on passe à la saison précédente.
                    if (lastEpisode.EpisodeNumber == 1)
                    {
                        lastSeason = client.GetTvSeason(idSerie, nbSeasons - reduceSeasonIndex);
                        reduceSeasonIndex++;
                        reduceEpisodeIndex = 1;
                    }
                    //On récupère l'épisode précédent s'il correspond niveau date
                    lastWatchedEpisode = lastSeason.Episodes[nbEpisodesLastSeason - reduceEpisodeIndex];
                    reduceEpisodeIndex++;
                }
                //A ce stade, on a le dernier épisode déjà diffusé, on souhaite celui juste après.

                //Si le futur épisode se trouve dans la saison en cours (saison non achevée), on prend l'épisode juste après dans la saison courante
                if (lastWatchedEpisode.EpisodeNumber < lastSeason.EpisodeCount)
                {
                    //Si l'épisode passe aujourd'hui, on conserve l'épisode courant
                    if (lastWatchedEpisode.AirDate.Date == DateTime.Now.Date)
                    {
                        lastEpisode = lastWatchedEpisode;
                    }
                        //Sinon on prend le suivant dans la liste
                    else
                    {
                        lastEpisode = lastSeason.Episodes[lastWatchedEpisode.EpisodeNumber]; //Pas +1 car attention aux indices des objets par rapport à la numérotation humaine
                    }
                }

                //Sinon, on doit prendre le premier épisode d'une nouvelle saison juste après la saison courante terminée
                if (lastWatchedEpisode.EpisodeNumber == lastSeason.EpisodeCount)
                {
                    lastCurrentSeason = client.GetTvSeason(idSerie, lastSeason.SeasonNumber + 1); ;
                    lastEpisode = lastCurrentSeason.Episodes[0];
                }
            }
            return lastEpisode; //A tester si c'est la date en cours pour la notif (cela vérifié que la série est encore d'actualité)
        }

        public void checkNewEpisode(int delay)
        {
            PackageHost.WriteInfo("counter : {0}", counter);
            //Si la liste n'est pas vide, on effectue la vérification du nouvel épisode tous les X secondes
            if (counter == delay)
            {
                if (this.ListFollowed.Count != 0)
                {
                    //Pour chaque série suivie par le client, on vérifie si un épisode passe dans la journée
                    foreach (var element in this.ListFollowed)
                    {
                        PackageHost.WriteInfo("Id de la serie : {0} name : {1}", element.Item1, element.Item2);
                        TvEpisode lastAiredEpisode = getDateLastEpisodeToWatch(element.Item1);
                        if (lastAiredEpisode.AirDate.Date == DateTime.Now.Date)
                        {
                            PackageHost.WriteInfo("An episode is airing today");
                            //Ajout des metadatas permettant de différencier les épisodes des séries suivies
                            Dictionary<string, string> metadatas = new Dictionary<string, string>();
                            metadatas.Add("isEpisode", "yes");
                            PackageHost.PushStateObject(element.Item2 + "Episode",
                                new
                                {
                                    titleSerie = element.Item2,
                                    seasonNumber = lastAiredEpisode.SeasonNumber,
                                    episodeNumber = lastAiredEpisode.EpisodeNumber,
                                    title = lastAiredEpisode.Name,
                                    summary = lastAiredEpisode.Overview,
                                    idEpisode = lastAiredEpisode.Id
                                },
                                "",
                                metadatas);
                        }
                    }
                }
                counter = 0;
            }
            counter = counter + 1; 
        }

        //Récupère les 20 séries les plus populaires
        [MessageCallback]
        public void getTopSeries()
        {
            if (MessageContext.Current.IsSaga) {
                TMDbClient client = connectToAPI();
                List<SearchTv> topSeries = new List<SearchTv>();
                topSeries = client.GetTvShowPopular().Results;
                int nbResults = topSeries.Count;
                foreach(SearchTv element in topSeries){
                    MessageContext.Current.SendResponse(new { TopSerie = element });
                }
            }
        }
    }
}
